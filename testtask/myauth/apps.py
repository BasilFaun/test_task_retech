from django.apps import AppConfig


class MyAuthConfig(AppConfig):
    name = 'myauth'
    verbose_name = 'My Authentication'
