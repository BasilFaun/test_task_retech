from rest_framework import viewsets,status
from .models import Todo
from .serializers import TodoSerializer
from organizations.models import Organization,OrganizationUser
from rest_framework.response import Response
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

class TodoViewSet(viewsets.ViewSet):
    def list(self, request):
        owner=request.session['organization_id']
        queryset = Todo.objects.filter(owner=owner)
        serializer = TodoSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, pk=None):
        owner=request.session['organization_id']
        queryset = Todo.objects.filter(owner=owner)
        todo = get_object_or_404(queryset, pk=pk)
        serializer = TodoSerializer(todo)
        return Response(serializer.data)

    def create(self, request):
        owner=request.session['organization_id']
        request.data['owner']=owner
        serializer = TodoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def update(self, request, pk=None):
        owner=request.session['organization_id']
        queryset = Todo.objects.filter(owner=owner)
        todo=get_object_or_404(queryset, pk=pk)
        serializer = TodoSerializer(todo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        owner=request.session['organization_id']
        queryset = Todo.objects.filter(owner=owner)
        todo=get_object_or_404(queryset, pk=pk)
        todo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

