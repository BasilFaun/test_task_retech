from django.conf.urls import url,include
#from rest_framework import routers
from .views import *

#router = routers.DefaultRouter()
#router.register(r'todos', TodoViewSet)

urlpatterns = [
    url('^todos/$', TodoViewSet.as_view({'get': 'list','post': 'create'}), name='todo-list'),
    url(r'^todos/(?P<pk>[0-9]+)/$', TodoViewSet.as_view(
                                   {'get':'retrieve',
                                    'put':'update',
                                    'delete': 'destroy'}), name='todo-detail'),
]