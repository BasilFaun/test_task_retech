from django.db import models
from organizations.models import Organization

class Todo(models.Model):
    owner = models.ForeignKey(Organization,related_name='todos')
    title = models.CharField(max_length=255,blank=True,null=True)
    description = models.TextField(blank=True,null=True)
    def __str__(self):
        return self.title