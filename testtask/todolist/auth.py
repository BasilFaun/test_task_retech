import base64
import binascii
from django.contrib.auth import get_user_model
from rest_framework import authentication,HTTP_HEADER_ENCODING, exceptions
from rest_framework.compat import authenticate
from organizations.models import Organization,OrganizationUser
from django.utils.translation import ugettext_lazy as _

class MyAuthentication(authentication.BasicAuthentication):
    def authenticate(self, request):
        if not 'HTTP_ORGANIZATION' in request.META:
            msg = _('For succesful authorization you must set the HTTP_ORGANIZATION header contained organization name')
            raise exceptions.AuthenticationFailed(msg)
        organization_name = request.META.get('HTTP_ORGANIZATION')
        try:
            organization=Organization.objects.get(name=organization_name)
        except Organization.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such organization')

        auth = authentication.get_authorization_header(request).split()
        if not auth or auth[0].lower() != b'basic':
            return None

        if len(auth) == 1:
            msg = _('Invalid basic header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid basic header. Credentials string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            auth_parts = base64.b64decode(auth[1]).decode(HTTP_HEADER_ENCODING).partition(':')
        except (TypeError, UnicodeDecodeError, binascii.Error):
            msg = _('Invalid basic header. Credentials not correctly base64 encoded.')
            raise exceptions.AuthenticationFailed(msg)

        userid, password = auth_parts[0], auth_parts[2]
        try:
            user=get_user_model().objects.get(email=userid)
        except get_user_model().DoesNotExist:
            msg = _('No such user')
            raise exceptions.AuthenticationFailed(msg)

        try:
            OrganizationUser.objects.get(user=user,organization=organization)
        except OrganizationUser.DoesNotExist:
            msg = _('No such user in organization %s' % organization_name)
            raise exceptions.AuthenticationFailed(msg)
        request.session['organization_id'] = organization.pk
        return self.authenticate_credentials(userid, password, request)

